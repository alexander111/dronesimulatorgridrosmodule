//////////////////////////////////////////////////////
//  droneSimulatorGridROSModule.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_SIMULATOR_GRID_ROS_MODULE_H
#define DRONE_SIMULATOR_GRID_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"



//OpenCV
#include <opencv2/opencv.hpp>



#include "droneSimulator.h"
#include "arenaSimulator.h"



//Subscribers: dronePoseStamped
#include "droneMsgsROS/dronePoseStamped.h"


//Publishers
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/points3DStamped.h"



const double DRONE_SIMULATOR_GRID_RATE = 10.0;



/////////////////////////////////////////
// Class DroneSimulatorGridROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneSimulatorGridROSModule : public Module
{	
    //Main classes
protected:
    //WORLD
    Arena IarcArena;

    //Drone
    Drone MyDrone;

    //exchange info
    std::vector<cv::Point3f> gridIntersections;


public:
    std::string sensorConfig;
    int setSensorConfig(std::string sensorConfig);


public:
    int setTopicConfigs(std::string dronePoseSubsTopicName, std::string gridIntersectionsPublTopicName);



    //subscribers
protected:
    //Topic name
    std::string dronePoseSubsTopicName;
    //Subscriber
    ros::Subscriber dronePoseSubs;
public:
    void dronePoseCallback(const droneMsgsROS::dronePoseStamped::ConstPtr& msg);



    //publishers
protected:
    //Topic name
    std::string gridIntersectionsPublTopicName;
    //Publisher
    ros::Publisher gridIntersectionsPubl;
protected:
    bool publishGridIntersections();




public:
    DroneSimulatorGridROSModule();
    ~DroneSimulatorGridROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};








#endif
