//////////////////////////////////////////////////////
//  geometry.h
//
//  Created on: Mar 25, 2014
//      Author: jl.sanchez
//
//  Last modification on: Mar 26, 2014
//      Author: jl.sanchez
//
//////////////////////////////////////////////////////



#ifndef GEOMETRY_H
#define GEOMETRY_H


#include <iostream>
#include <string>
#include <vector>


#include "opencv2/opencv.hpp"




/////////////////////////////////////////
// Template Class Line
//
//   Description
//
/////////////////////////////////////////
template <class PointType>
class Line
{
protected:
    std::vector<PointType> points;


public:
    Line();
    ~Line();

public:
    int setPoints(const PointType& point1, const PointType& point2);
    int getPoints(PointType& point1, PointType& point2);


};


template <class PointType>
Line<PointType>::Line() :
    points(2)
{

    return;
}

template <class PointType>
Line<PointType>::~Line()
{

    return;
}

template <class PointType>
int Line<PointType>::setPoints(const PointType &point1, const PointType &point2)
{
    this->points[0]=point1;
    this->points[1]=point2;

    return 1;
}

template <class PointType>
int Line<PointType>::getPoints(PointType& point1, PointType& point2)
{
    point1=this->points[0];
    point2=this->points[1];

    return 1;
}
















#endif
